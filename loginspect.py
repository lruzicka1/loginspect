#!/usr/bin/python

# Log Inspect - this program parses the openQA autoinst-log.txt and shows
# some valuable pieces of information to help the operator troubleshoot
# the failures.
#
# Copyright Red Hat, 2024
#
# This file is part of os-autoinst-distri-fedora.
#
# os-autoinst-distri-fedora is free software; you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Author: Lukáš Růžička <lruzicka@redhat.com>

import argparse
import re
import sys

class Logfile:
    def __init__(self, logfile):
        self.logfile = logfile
        self.tests = {}
        self.steps = {}
        testname = ""
        multiline = ""
        started = False
        incomplete = False
        for line in self.logfile:
            # We look for a special combination ||| that indicates start
            # and stop of the single test cases.
            pattern = r"],"
            if "|||" in line:
                # Search patterns in the line:
                # |||, starting (or finished), testname, testpath
                columns = re.search(r"\|\|\|(\s\w*\s)(\w*)(.*)", line)
                # When we find the starting point of a testcase
                if columns.groups()[0].strip() == 'starting':
                    # We only want to collect lines that belong
                    # to a particular test, so we want to know
                    # that we have started.
                    started = True
                    # Read its name
                    testname = columns.groups()[1]
                    # If it is not in self.tests, add it.11
                    if testname not in self.tests.keys():
                        self.tests[testname] = []
                elif columns.groups()[0].strip() == 'finished':
                    started = False
                    testname = ""
            # When we find a query/response line:
            elif re.search(r"<<<|>>>", line):
                if started and testname:
                    pattern = r"mustmatch=\["
                    if re.search(pattern, line):
                        multiline = line.strip()
                        incomplete = True
                    else:
                        self.add_to_values(testname, line)
            # When we find a basetest line
            elif re.search(r"\s:::\s", line):
                if "Test died" in line:
                    self.add_to_values(testname, line)
                    #break
                elif started and testname:
                    self.add_to_values(testname, line)
            # When a multiline ends
            elif re.search(pattern, line):
                incomplete = False
                multiline += line.strip()
                self.add_to_values(testname, multiline)
                multiline = ""
            # In other cases
            else:
                # When we are incomplete and the multiline
                # seems to close
                if incomplete:
                    multiline += line.strip()
                else:
                    pass

    def add_to_values(self, key, value):
        """ Adds a record to values and checks for duplicities in keys. """
        if key in self.tests:
            values = self.tests[key]
            values.append(value)
            self.tests[key] = values
        else:
            self.tests[key] = [value]

    def available_tests(self):
        tests = list(self.tests.keys())
        return tests

    def test_lines(self, test):
        lines = self.tests[test]
        return lines

    def find_steps(self, test):
        testlines = self.test_lines(test)
        denylist = ["wait_screen_change",""]
        steps = []
        for step in testlines:
            step = step.split('::')[1]
            pattern = r"(\w\w*)(\(|:\s\()(.*)\)"
            columns = re.search(pattern, step)
            if columns:
                command = columns[1]
                if command in denylist:
                    continue
                args = columns[3]
                args = args.split(",")
                parsed = {command:args}
                steps.append(parsed)
                columns = None
            elif "handle_found_needle" in step:
                target = step.split(':')
                target = target[1].strip()
                parsed = {"complete": [target]}
                steps.append(parsed)
            elif "check_backend_response" in step:
                target = step.split(':')
                target = target[1].strip()
                parsed = {"incomplete": [target]}
                steps.append(parsed)
            else:
                pass

        self.steps = steps
        return self.steps

    def find_death(self):
        data = self.tests.items()
        error = ""
        result = []
        for item in data:
            for line in item[1]:
                if "Test died" in line:
                    split = line.split('#')
                    error = split[1].strip()

        result.append((item[0], error))
        return result

    def display_workflow(self, error):
        counter = 1
        for step in self.steps:
            if "incomplete" in step.keys():
                etext = str(step['incomplete'])
                etag = self.parse_error(error)
                if etag in etext:
                    print(" ")
                    print("The test died at this point!")
                    print(f"Expected needle ({etag}) has not be saturated.")
                    break
            print(f"--- Step {counter} ---------------")
            key = list(step.keys())[0]
            print("        ",key)
            for arg in step[key]:
                print("            ", arg.strip())
            counter += 1

    def needle_checks(self):
        counter = 1
        penultimate = ""
        successful = {}
        unsuccessful = {}
        for step in self.steps:
            key = list(step.keys())[0]
            key = key.strip()
            if key in ["check_screen", "assert_screen"]:
                content = self.find_tag(step[key])
                #print(f"{counter} {key} for {content}")
                penultimate = f"{key}: {content}"
            elif key == "complete" and penultimate:
                match = self.find_match(step[key])
                successful[counter] = [penultimate, match]
                penultimate = ""
            elif key == "incomplete" and penultimate:
                match = self.find_match(step[key])
                unsuccessful[counter] = [penultimate, match]
                penultimate = ""
            else:
                pass
            counter += 1
        return (successful, unsuccessful)

    def failed_tests(self):
        pass

    def find_tag(self, value):
        """ Takes the value line and returns the name of the tag. """
        # Take the first part of the value and split on ','
        tag = value[0].split(",")
        # Take the first part
        tag = tag[0]
        # Split that on '='
        tag = tag.split("=")
        # And take the second part.
        tag = tag[1]
        return tag

    def find_match(self, value):
        match = value[0]
        match = match.split(" ")
        if "found" in match:
            match = match[1]
        elif "timed" in match:
            match = match[0]
            match = match.split("=")
            match = match[1]
        return match

    def display_steps(self):
        for step in self.steps:
            action = step[0]
            print(action)

    def parse_error(self, error):
        pattern = r"'(\S*)'"
        match = re.search(pattern, error)
        tag = match.groups()[0]
        return tag



def cli_args():
    parser = argparse.ArgumentParser(prog='Log Inspector', description='Check openQA logs', epilog='Good Bye.')
    parser.add_argument('-f', '--file', help="The file to inspect.", required = True)
    parser.add_argument('-s', '--scenario', help="Select the scenario.")
    parser.add_argument('-t', '--test', help="Which test should be examined if there are more.")

    return parser.parse_args()


def main():

    args = cli_args()

    LOGFILE = args.file

    with open(LOGFILE) as logfile:
        data = logfile.readlines()

    log = Logfile(data)
    errors = log.find_death()

    if args.test:
        error = None
        for e in errors:
            if e[0] == args.test:
                error = e
    else:
        error = errors[0]

    try:
        steps = log.find_steps(error[0])
    except TypeError:
        print(f"No such test has been found in the log file {args.file}")
        sys.exit(10)

    if args.scenario == 'incomplete':
        print("--- Incomplete needle checks: -------------------------")
        successful, failed = log.needle_checks()
        for f in failed.keys():
            print("    ", f"{f}: {failed[f]}")
    elif args.scenario == 'tests':
        tests = log.available_tests()
        print("--- Available tests: -------------------------")
        for test in tests:
            print("    ", test)
    elif args.scenario == 'errors':
        print("--- Failed tests: -------------------------")
        for e in errors:
            print("    ", e[0])
    elif args.scenario == 'steps':
        print("--- Script steps: -------------------------")
        log.display_workflow(error[1])
    elif args.scenario == 'lines':
        print(f"--- Log from {args.test} -------------------------")
        lines = log.test_lines(args.test)
        for line in lines:
            print("    ", line)

    else:
        print(f"The test died in the {error[0]} module due to the following reason:\n    {error[1]}\n")


if __name__ == "__main__":
    main()
