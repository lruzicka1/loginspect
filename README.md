# Log Inspect

**Log Inspect** is a Python script that should help to go quickly over the `autoinst-log.txt` file and get the most important information based on a selected scenario.

## Usage

`./loginspect.py -f <input_file> -s <scenario> -t <selected_test>`

### Example 

Running `./loginspect.py -f <input_file>` without other arguments will show the reason of failure and the location in the test case.

### CLI arguments

* `-f`, `--file` loads the log file
* `-t`, `--test` selects a test if there are more test cases covered by the log file
* `-s`, `--scenario` affects which information will be shown by the file

### Available scenarios

* *tests* will list all available test cases that you could inspect in this log file
* *lines* will return all relevant lines from a given test case, this is useful to export parts of the log file to other services, or for model training. Note, that part of the messages have been thrown away at this point, so the info will not match the original file.
* *steps* will show a list of test case steps with their settings, e.g. an assertion with a location to click, etc.
* *errors* will show all modules where a critical error appeared (the test died).
* *incomplete* will show all needles that were not satisfied during the test, which is super useful to troubleshoot the *check_screen* needles that will not fail the test per se, but divert the course of the test case which leads to a weird failure somewhere else.

### Issues

The script might have bugs and produce incomplete results. If you find a discrepancy, let me know in the issues, or open a pull request with a fix suggestion.

